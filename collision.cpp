#include <SFML/Graphics.hpp>
#include <iostream>
#include "windows.h"
#include <string>
#include <sstream>
using namespace std;

std::string IStr(int a)
{
    ostringstream temp;
    temp<<a;
    return temp.str();
}


using namespace sf;

int player_size = 32;
int speed = 20000;
float player_x = 0;
float player_y = 0;
float dump_player_x = 0;
float dump_player_y = 0;
int block_size = 32;

bool coll_on = false;

#include "map_controll.cpp"


map <string,int> MyMap;

//Code keys
int latest_key = 0; // 0 - nothing, 1 - top, 2 - bottom, 3 - left, 4 - right.



std::string name_map;

bool collision(float x1, float y1, int size1, float x2, float y2, int size2){
	bool XColl=false;
	bool YColl=false;
	
	if ((x1 + size1 >= x2) && (x1 <= x2 + size2)) XColl = true;
	if ((y1 + size1 >= y2) && (y1 <= y2 + size2)) YColl = true;

	if (XColl&YColl){return true;}
	return false;
}

bool collision_around(int mode, int dir){
	int hyrth=0;
	
	cout << player_x << " " << player_y << endl;
	
	for(int y=player_y/32;y<(player_y+32)/32;y++){
		for(int x=player_x/32;x<(player_x+32)/32;x++){
			if(MyMap[IStr(x)+":"+IStr(y)] == 1){ //Block
				
				if(mode == 0){
					
					if(dir == 0) player_x = x*32 + 32;
					else if(dir == 1) player_x = x*32-player_size;
					hyrth++;
					cout << player_x << " " << player_y << endl;
				}else if(mode == 1){
					
					if(dir == 0) player_y = y*32 - 32;
					else if(dir == 1) player_y = y*32+player_size;
					hyrth++;
					cout << player_x << " " << player_y << endl;
				}
				
				
				cout << "Detected block at " << y << " " << x << endl;
				
				
			}
		}
	}
	
	if(hyrth > 0) return true;
	return false;
	
	//cout <<  << endl;
	
	//if(collision(x*block_size,y*block_size,block_size,player_x,player_y,player_size)){
	//	coll_on = true;
	//}else coll_on = false;
	
	//return coll_on;
}

int main(){
	
	
	MyMap = select_map();
	
	player_x = pos_player_x*32+1;
	player_y = pos_player_y*32+1;
	
	//RenderWindow window(VideoMode(400,400), "Test!");
	
	RenderWindow window(VideoMode(400,400), "Test!");
	
	//display_map();
	
	Texture t;
	t.loadFromFile("res/user/user-0.png");
	
	Sprite s;
	s.setTexture(t);
	s.setPosition(player_x,player_y);
	
	
	
	
	Clock clock;
	
	while(window.isOpen()){
		
		float time = clock.getElapsedTime().asMicroseconds();
		clock.restart();
		time = time/speed;
		
		window.clear();
		
		Event event;
		while(window.pollEvent(event)){
			if(event.type == Event::Closed)
				window.close();
		}
		
		for (auto it = MyMap.begin(); it != MyMap.end(); ++it){
			//cout << (*it).first << " : " << (*it).second << endl;
			
			auto v = explode((*it).first, ':');
			int x = atoi( v[0].c_str() );
			int y = atoi( v[1].c_str() );
			
			sf::Texture t2;
			if((*it).second == 1){ //Block
				
				t2.loadFromFile("res/block.png");
			}else if((*it).second == 0) // dirt
				t2.loadFromFile("res/dirt.png");
			
			
			
			
			
			sf::Sprite s2;
			s2.setTexture(t2);
			s2.setPosition(x*32,y*32);
			window.draw(s2);
		}
		
		
		
		dump_player_x = player_x;
		dump_player_y = player_y;
		
		
		/* Detecting keyboard press */
		if (Keyboard::isKeyPressed(Keyboard::Left)){
			latest_key = 3;
			//If pressed key Left:
			//s.move(-0.1*time, 0);
			if(!collision_around(0, 0)) player_x = player_x-(1*time);
			
			t.loadFromFile("res/user/user-2.png");
			//latest_key = 0;
			
		}
		if (Keyboard::isKeyPressed(Keyboard::Right)){
			latest_key = 4;
			//If pressed key Right:
			//s.move(0.1*time, 0);
			if(!collision_around(0, 1)) player_x = player_x+(1*time);
			
			t.loadFromFile("res/user/user-3.png");
			//latest_key = 0;
		}
		if (Keyboard::isKeyPressed(Keyboard::Up)){
			latest_key = 1;
			//If pressed key Top:
			//s.move(0, -0.1*time);
			if(!collision_around(1, 1)) player_y = player_y-(1*time);
			
			t.loadFromFile("res/user/user-1.png");
			//latest_key = 0;
		}
		if (Keyboard::isKeyPressed(Keyboard::Down)){
			latest_key = 2;
			//If pressed key Bottom:
			//s.move(0, 0.1*time);
			if(!collision_around(1, 0)) player_y = player_y+(1*time);
			
			t.loadFromFile("res/user/user-0.png");
			//latest_key = 0;
		}
		
		s.setPosition(player_x,player_y);
		
		
		
		/* Drawing */
		window.draw(s);
		//window.draw(s2);
		window.display();
	}
	
	return 0;
}