#include <SFML/Graphics.hpp>
#include <vector>
#include <string>

class Shark{
	private:
		std::vector<sf::Texture> textures;
		float shark_x, shark_y = 0;
		sf::Sprite shark_sprite;
		int shark_speed = 12000;
		int state_x,state_y = 0;
		int rand_percent = 3000;
		int attack_x,attack_y = 64;

		
		int size_x,size_y = 0;
		int health = 100;
		//int health_block = 100;
		int state_block = -1;
		bool temp=false;
		
		
		
	public:
		int frames_attack = 300;
		int current_frame = 0;
		bool isAttack = false;
		Shark(){
			state_y = 1;
			textures.reserve(5);
			textures[0].loadFromFile("res/enemy/shark-0.png");
			shark_sprite.setTexture(textures[0]);
			shark_sprite.setTexture(shark_texture);
		}
		void tick(int time, map <string,int> MyMap){
			int x = shark_x/32;
			int y = shark_y/32;
			
			if(!isAttack){
				for (int i=0;i<size_y;i++){
					for (int j=0;j<size_x;j++){
						if(MyMap[std::to_string(i)+":"+std::to_string(j)] != 1){
							if((rand() % rand_percent) == 100){
								std::cout << " LOL " << std::endl;
								isAttack = true;
								attack(j,i);
							}
							break;
						}
					}
				}
			}else{
				attack(attack_x,attack_y);
			}
					
				
			
			
		}
		void setSizeMap(int sx, int sy){
			size_x = sx;
			size_y = sy;
		}
		void attack(int x, int y){
			if(isAttack && health>0){
				//std::cout << "Init Shark TRUE " << x << " " << y << std::endl;
				if(attack_x == -1 || attack_y == -1){
					attack_x = x;
					attack_y= y;
					current_frame = 0;
				}else if(current_frame <= frames_attack){
					current_frame++;
				}else if(current_frame >= frames_attack && !temp){
					temp=true;
					state_block = 1;
				}else{
					current_frame = 0;
					isAttack = false;
					attack_x = -1;
					attack_y= -1;
					state_block = -1;
					temp = false;
				}
			}else{
				health = 100;
				isAttack == false;
				current_frame = 0;
				attack_x = -1;
				attack_y= -1;
				state_block = -1;
				temp = false;
			}
		}
		sf::Sprite get(){
			shark_sprite.setPosition((attack_x-1)*32+16,attack_y*32);
			
			return shark_sprite;
		}
		int getAttackX(){
			return attack_x;
		}
		int getAttackY(){
			return attack_y;
		}
		int getState(){
			return state_block;
		}
		void setHealth(int h){
			health = h;
		}
		int getHealth(){
			return health;
		}
	
};