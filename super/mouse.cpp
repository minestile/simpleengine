
class MouseEngine{
	private:
		float mouse_x, mouse_y = 0;
		int rel_mouse_x, rel_mouse_y = 0;
	public:
		bool right = false;
		bool left = false;
		void update(float x, float y){
			if(x > 500 || y > 500 || x < 0 || y < 0){
				
			}else{
				mouse_x = x;
				mouse_y = y;
				rel_mouse_x = (( (int) x) / 32);
				rel_mouse_y = (( (int) y) / 32);
			}
		}
		float x(){
			return mouse_x;
		}
		float y(){
			return mouse_y;
		}
		int rx(){
			return rel_mouse_x;
		}
		int ry(){
			return rel_mouse_y;
		}
		void clickEvent(int mode){
			if(mode == 0){ // Левая кнопка мыши
				left = true;
			}else if(mode == 1){ // Правая кнопка мыши
				right = true;
			}else if(mode == 2){ // Если 2, то обнуляем все
				right = false;
				left = false;
			}
		}
};