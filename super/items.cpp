#include <SFML/Graphics.hpp>
#include <vector>
#include <string>

class Items{
	std::vector<int> types;
	std::vector<int> specs;
	std::vector<std::string> texts;
	std::vector<std::string> descs;
	int count = 6;
	int temp = 0;
	public:
		Items(){
			types.reserve(count);
			specs.reserve(count);
			texts.reserve(count);
			descs.reserve(count);
			
			
			add(0,0,"item/seaweed.png","Seaweed");
			add(1,0,"item/wood.png","Wood");
			add(2,0,"web.png","Web");
			add(3,1,"item/spear.png","Spear");
			add(4,0,"item/stick.png","Stick");
			add(5,0,"item/string.png","String");
			
		}
		int getType(int i){
			return types[i];
		}
		int getSpec(int i){
			return specs[i];
		}
		std::string getDescription(int i){
			return descs[i];
		}
		std::string getTexture(int i){
			if(i == -1)
				return "";
			return texts[i];
		}
		void add(int type, int spec, std::string texture_path, std::string txt="Item"){
			types.push_back(type);
			specs.push_back(spec);
			texts.push_back("res/"+texture_path);
			descs.push_back(txt);
			
		}
		int getCount(){
			return count;
		}
			
};