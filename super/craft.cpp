#include <vector>
class Craft{
	private:
		int arr[9];
		std::vector <std::vector<int>> recipes;
		int count_recipes = 4;
		int id_item_attach = -1;
	public:
		Craft(){
			clear();
			recipes.reserve(count_recipes);
			
			std::vector <int> recipe(11);
			
			//Копье
			recipe[0] = -1;
			recipe[1] = -1;
			recipe[2] = 4;
			recipe[3] = -1;
			recipe[4] = 4;
			recipe[5] = -1;
			recipe[6] = 4;
			recipe[7] = -1;
			recipe[8] = -1;
			recipe[9] = 3;
			recipe[10] = 1;
			recipes.push_back(recipe);
			
			//Палка
			recipe[0] = -1;
			recipe[1] = -1;
			recipe[2] = -1;
			recipe[3] = -1;
			recipe[4] = 1;
			recipe[5] = -1;
			recipe[6] = -1;
			recipe[7] = -1;
			recipe[8] = -1;
			recipe[9] = 4;
			recipe[10] = 4;
			recipes.push_back(recipe);
			
			//Нить
			recipe[0] = -1;
			recipe[1] = -1;
			recipe[2] = -1;
			recipe[3] = -1;
			recipe[4] = 0;
			recipe[5] = -1;
			recipe[6] = -1;
			recipe[7] = -1;
			recipe[8] = -1;
			recipe[9] = 5;
			recipe[10] = 2;
			recipes.push_back(recipe);
			
			//Сеть
			recipe[0] = 4;
			recipe[1] = 4;
			recipe[2] = 4;
			recipe[3] = 5;
			recipe[4] = 5;
			recipe[5] = 5;
			recipe[6] = 5;
			recipe[7] = 5;
			recipe[8] = 5;
			recipe[9] = 2;
			recipe[10] = 1;
			recipes.push_back(recipe);
		}
		int getIdItem(int x, int y){
			int i = x+(y*3);
			return arr[i];
		}
		void setIdItem(int x, int y, int type){
			int i = x+(y*3);
			arr[i] = type;
		}
		void setIdAttach(int id){
			id_item_attach = id;
		}
		int getIdAttach(){
			return id_item_attach;
		}
		void clear(){
			for (int i=0;i<9;i++)
				arr[i] = -1;
		}
		std::vector <int> craft(int type=0){
			std::vector <int> res;
			res.reserve(3);
			bool Ok = false;
			for (int i=0;i<count_recipes;i++){
				std::vector<int> recipe = recipes[i];
				int match = 0;
				for (int j=0;j<9;j++){
					if(arr[j] == recipe[j]){
						match++;
					}
				}
				if(match == 9){
					res.push_back(recipe[9]);
					res.push_back(recipe[10]);
					res.push_back(i);
					Ok = true;
				}
			}
			if (Ok){
				return res;
			}else{
				res.push_back(-1);
				res.push_back(-1);
				res.push_back(-1);
				return res;
			}
		}
		int getCountFromRecipe(int i){
			return recipes[i][10];
		}
};