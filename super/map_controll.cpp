#include <iostream>
#include <fstream>
#include <SFML/Graphics.hpp>
#include "windows.h"

using namespace std;

string map_name;
int map_size_x;
int map_size_y;
float pos_player_x;
float pos_player_y;
map <string,int> Map;

const vector<string> explode(const string& s, const char& c)
{
	string buff{""};
	vector<string> v;
	
	for(auto n:s)
	{
		if(n != c) buff+=n; else
		if(n == c && buff != "") { v.push_back(buff); buff = ""; }
	}
	if(buff != "") v.push_back(buff);
	
	return v;
}

map<string,int> load_map (string name){
    ifstream file(name.c_str());
    if (!file) {
        throw runtime_error("Can't open file " +name);
    }

    string line;
    vector<string> lines;
	int inc = 0;
	
    while (getline(file, line)) {
        try {
			if(inc==0){ // size map
				
				auto v = explode(line, ' ');
				map_size_x = atoi( v[0].c_str() );
				map_size_y = atoi( v[1].c_str() );
				
				cout << "Loaded box: " << map_size_x << "x" << map_size_y << endl;
			}else if(inc==1){ //position player
				auto v = explode(line, ' ');
				pos_player_x = atoi( v[0].c_str() );
				pos_player_y = atoi( v[1].c_str() );
				
				
				
				cout << "Loaded player at: " << pos_player_x << " " << pos_player_y << endl;
			}else{ //Map
				auto v = explode(line, ' ');
				string coords = v[0].c_str();
				int type = atoi( v[1].c_str() );
				Map.insert ( pair<string,int>(coords,type) );
			}
			
			
            lines.push_back(line);
        } catch (...) {
            file.close();
            throw;
        }
		inc++;
    }
    file.close();
	return Map;
}



map<string,int> select_map(){
	
	cout << "Enter name .mst file (map): ";
	//cin >> map_name;
	
	map_name="map.mst";
	
	cout << "Load "+map_name+" ..." << endl;
	
	if(map_name == "n") return load_map("map.mst");
	return load_map(map_name);
}

void save(map <string,int> MyMap){
	cout << "Open file " << map_name << "." << endl;
	
	ofstream myfile;
	myfile.open (map_name);
	
	cout << "Saving map size... " << endl;
	myfile << map_size_x << " " << map_size_x << "\n";
	
	cout << "Saving player postition ..." << endl;
	myfile << pos_player_x << " " << pos_player_x << "\n";
	
	int i=2;
	
	for (auto it = MyMap.begin(); it != MyMap.end(); ++it){
		string coords = (*it).first;
		int type = (*it).second;
		myfile << coords << " " << type << "\n";
		
		i++;
	}
	
	cout << "Ended successful! Wrote " << i << " rows." << endl;
}