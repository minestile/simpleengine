#include <SFML/Graphics.hpp>
#include <vector>

using namespace sf;

Texture wood_texture;
Texture seaweed_texture;
Texture select_texture;
Texture select_texture_on;
Texture inv_texture;
Texture web_texture;
Texture spear_texture;
Texture shark_texture;




class textureManager{
	public:
		int count = 0;
		std::vector<std::string> paths;
	public:
		textureManager(int count){
			paths.reserve(count);
		}
		std::string get(int type){
			return paths[type];
		}
		void add(std::string path){
			paths[count] = path;
			count++;
		}
};

textureManager texman(9);

void texture_load_all(){
	wood_texture.loadFromFile("res/item/wood.png");
	select_texture.loadFromFile("res/select.png");
	select_texture_on.loadFromFile("res/select-on.png");
	seaweed_texture.loadFromFile("res/item/seaweed.png");
	inv_texture.loadFromFile("res/inv-item.png");
	web_texture.loadFromFile("res/web.png");
	spear_texture.loadFromFile("res/item/spear.png");
	shark_texture.loadFromFile("res/enemy/shark-0.png");
	
	texman.add("res/item/seaweed.png");
	texman.add("res/item/wood.png");
	texman.add("res/web.png");
	
	texman.add("res/select.png");
	texman.add("res/select-on.png");
	texman.add("res/item/seaweed.png");
	texman.add("res/inv-item.png");
	texman.add("res/item/wood.png");
	texman.add("res/item/spear.png");
	
}
