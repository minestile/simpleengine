#include <SFML/Graphics.hpp>
#include <iostream>
#include <string>
#include <sstream>
#include <map>

using namespace std;

std::string IStr(int a)
{
    ostringstream temp;
    temp<<a;
    return temp.str();
}

using namespace sf;



class Player {
	private:
		float player_x, player_y = 0;
		//bool is_collision;
		Sprite player_sprite;
		Texture player_texture;
		float speed = 1;
		int map_size_y, map_size_x;
		
	public:
		Player(std::string pt){
			player_texture.loadFromFile(pt);
			player_sprite.setTexture(player_texture);
			player_sprite.setPosition(player_x,player_y);
		}
		void move(float px,float py){
			player_x = player_x-px;
			player_y = player_y-py;
			
			player_sprite.setPosition(player_x,player_y);
		}
		void setPosition(float x, float y){
			setX(x);
			setY(y);
		}
		Sprite get(){
			player_sprite.setPosition(player_x,player_y);
			return player_sprite;
		}
		
		float x(){ return player_x; }
		float y(){ return player_y; }
		void setX(float px){ player_x = px; }
		void setY(float py){ player_y = py; }
		
		void right(int time, map<string,int> MyMap){ 
			if(!is_collision(MyMap,time,3)) move(-(time/speed)*1,0);
			setSprite("res/user/user-3.png"); 
		}
		void left(int time, map<string,int> MyMap){
			if(!is_collision(MyMap,time,4)) move((time/speed)*1,0);
			setSprite("res/user/user-2.png");
		}
		void down(int time, map<string,int> MyMap){
			if(!is_collision(MyMap,time,2)) move(0,-(time/speed)*1);
			setSprite("res/user/user-0.png");
		}
		void up(int time, map<string,int> MyMap){
			if(!is_collision(MyMap,time,1)) move(0,(time/speed)*1);
			setSprite("res/user/user-1.png");
		}
		void setSpeed(int sp){ speed = sp; }
		
		void setSprite(std::string pt){
			player_texture.loadFromFile(pt);
		}
		void setMapSize(int x,int y){
			map_size_y = y;
			map_size_x = x;
		}
		bool is_collision(map<string,int> MyMap, int time, int mode){
			int hyrth=0;
			int hy=y()+16;
			int hx=x()+16;
			
			
			if(mode==1) hy = y()-(time/speed); //Up
			else if(mode==2) hy = (y()+32)+(time/speed); //down
			else if(mode==3) hx = (x()+32)+(time/speed);  //right
			else if(mode==4) hx = x()-(time/speed); //left
			
			for(int py=hy/32;py<(hy+32)/32;py++){
				for(int px=hx/32;px<(hx+32)/32;px++){
					if(MyMap[IStr(py)+":"+IStr(px)] == 1){ //Block	
						hyrth++;
					}
				}
			}
					
			//Не выпускаем игрока за экран и в воду :)
			if(hyrth > 0 || hy >= map_size_y*32 || hx >= map_size_x*32 || hx <= 0 || hy <= 0) return true;
			return false;
		}

};