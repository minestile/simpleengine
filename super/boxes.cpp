

class Boxes{
	private:
		std::string text_d = "";
		int x = 0;
		int y = 0;
		Font font;
	public:
		Boxes(){
			font.loadFromFile("res/OpenSans-Bold.ttf");
		}
		void setup(int xr, int yr, std::string txt){
			text_d = txt;
			x = xr;
			y = yr;
		}
		void show (sf::RenderWindow *window){
			//sf::Texture box_texture;
			//box_texture.loadFromFile("res/box.png");
			//sf::Sprite box;
			//box.setPosition(x,y);
			int count_chars = 0;
			const char* str = text_d.c_str();
			while (*str){
				*str++;
				count_chars++;
			}
			
			sf::RectangleShape box(sf::Vector2f(count_chars*12, 20));
			box.setFillColor(sf::Color(56, 56, 56));
			box.setPosition(x,y);
			box.setOutlineThickness(2);
			box.setOutlineColor(sf::Color(0, 132, 255));
			//box.setTexture(box_texture);
			window->draw(box);
			
			sf::Text text;
			text.setFont(font);
			text.setString(text_d);
			text.setPosition(x+5,y+4);
			text.setCharacterSize(12);
			text.setFillColor(sf::Color::White);
			window->draw(text);
		}
};