#define __USE_MINGW_ANSI_STDIO 0

#include <SFML/Graphics.hpp>
#include <iostream>
#include "windows.h"
#include "texturemanager.cpp"
#include "player.cpp"
#include "map_controll.cpp"
#include "items.cpp"
#include "shark.cpp"
#include "inv.cpp"
#include "mouse.cpp"
#include "keyboard.cpp"
#include "web.cpp"
#include "craft.cpp"
#include "boxes.cpp"

#include <string>



using namespace sf;

#include "genItems.cpp"

//Без этого не работает, сорри (работает? не трогай!)
int map_gen_x = 0;
int map_gen_y = 0;
int inv_iter=0;

//Инициализация необходимых классов
Player player("res/user/user-0.png"); //Класс игрока
GenItems genItems; //Класс генерации предметов
Inv inv; //Класс инвентаря
MouseEngine mouse; //Класс для работы с мышью
KeyBoard keyboard; //Класс для работы с клавиатурой
Webs webs; //Сети (ух, сложно все с ними было ))) )
Shark shark; //Акула
Items items; //Предметы
Craft craft;

map <string,int> MyMap;
map <string,int> objsMap; //Массив для обьектов, они будут прорисованы поверх карты.
int window_height = 500;
int window_width = 500;




Font font;



int main(){
	float time = 0;
	
	
	//Добавление текстур
	texture_load_all();
	
	font.loadFromFile("res/OpenSans-Bold.ttf");
	
	player.setSpeed(4200);
	
	MyMap = select_map();
	
	//Первичная настройка классов
	player.setMapSize(map_size_x, map_size_y);
	inv.start();
	shark.setSizeMap(map_size_x,map_size_y);
	
	int map[map_size_x][map_size_y]; //Массив для рисования карты
	
	
	
	//for(int i = 0; i < map_size_y; i++)
	//	for(int j = 0; j < map_size_x; j++)
	//		objsMap[to_string(j)+":"+to_string(i)] = 0;
	
	// Преобразование в нормальный массив, а не Map
	for (auto it = MyMap.begin(); it != MyMap.end(); ++it){
		auto v = explode((*it).first, ':');
		int y = atoi( v[0].c_str() );
		int x = atoi( v[1].c_str() );
		
		map[x][y] =  (int) (*it).second;
		
		//cout << map[x][y] << " - " << (*it).first << endl;
	}
	
	player.setPosition(pos_player_x*32, pos_player_y*32);
	
	RenderWindow window(VideoMode(window_height,window_width), "Test!");
	
	
	cout << player.x() << player.y() << endl;
	
	genItems.reserve(10,map_size_y);
	
	
	
	//Чит, чтобы дебажить было легче.
	bool cheat =true;
	
	if(cheat){
		inv.add(0,0,99);
		inv.add(1,1,99);
	}
	

	
	Clock clock;
	
	while(window.isOpen()){
		time = clock.getElapsedTime().asMicroseconds();
		clock.restart();
		
		window.clear();
		
		webs.update(objsMap); //Обновляем стату о сетях
		
		shark.tick(time,MyMap); //Один тик для акулы
		
		//Проверка, сломала ли акула какой-нибудь блок
		if(shark.getState() != -1){
			map[shark.getAttackX()][shark.getAttackY()] = shark.getState();
			MyMap[to_string(shark.getAttackY())+":"+to_string(shark.getAttackX())] = shark.getState();
			objsMap[to_string(shark.getAttackX())+":"+to_string(shark.getAttackY())] = 0;
			
		}
		
		
		//Очистка событий
		keyboard.event(0);
		mouse.clickEvent(2);
		
		Event event;
		while(window.pollEvent(event)){
			if(event.type == Event::Closed)
				window.close();
			
			if (event.type == Event::MouseButtonPressed){
				if (event.key.code == Mouse::Left) mouse.clickEvent(0);
				if (event.key.code == Mouse::Right) mouse.clickEvent(1);
			}
			if (event.type == sf::Event::TextEntered)
				keyboard.event(event.text.unicode);
		}
		
		//Работа с мышью (обновление данных)
		Vector2i pixelPos = Mouse::getPosition(window);
		Vector2f pos = window.mapPixelToCoords(pixelPos);
		mouse.update(pos.x, pos.y);
		
		
		
		//Перемещение персонажа
		if (Keyboard::isKeyPressed(Keyboard::Left)) player.left(time, MyMap);
		if (Keyboard::isKeyPressed(Keyboard::Right)) player.right(time, MyMap);
		if (Keyboard::isKeyPressed(Keyboard::Up)) player.up(time, MyMap);
		if (Keyboard::isKeyPressed(Keyboard::Down)) player.down(time, MyMap);
		
		//Отрисовка карты
		for(map_gen_y = 0; map_gen_y < map_size_y; map_gen_y++)
			for(map_gen_x = 0; map_gen_x < map_size_x; map_gen_x++){
				
				
				//Оптимизация
				if(map_gen_x > window_width/32 || map_gen_y > window_height/32) break;
				
				sf::Texture t2,obj_texture,adding_texture;
				int type = map[map_gen_x][map_gen_y];
				if(type == 0)
					t2.loadFromFile("res/wood.png");
				else if(type == 1)
					t2.loadFromFile("res/water.png");	
				
				//Доп. обьекты (выше карты)
				int type_obj = objsMap[to_string(map_gen_x)+":"+to_string(map_gen_y)];
				if(type_obj == 2){
					obj_texture.loadFromFile("res/web.png");
					
					//cout << " Count " << webs.getCount(map_gen_x, map_gen_y) << endl;
					
					if(webs.getCount(map_gen_x, map_gen_y) > 0){
						adding_texture.loadFromFile(texman.get(webs.getType(map_gen_x, map_gen_y)));
						//cout << texman.get(webs.getType(map_gen_x, map_gen_y)) << endl;
						
					}
				}
				
				sf::Sprite s2,obj,adding_obj;
				s2.setTexture(t2);
				obj.setTexture(obj_texture);
				s2.setPosition(map_gen_x*32,map_gen_y*32);
				obj.setPosition(map_gen_x*32,map_gen_y*32);
				adding_obj.setTexture(adding_texture);
				adding_obj.setPosition(map_gen_x*32,map_gen_y*32);
				window.draw(s2);
				window.draw(obj);
				window.draw(adding_obj);
				
				
			}
		
		window.draw(player.get()); //Отрисовка игрока
		
		if(rand() % 64673 == 4533){
			int rand_dir = rand() % 2;
			genItems.setDirection(rand_dir);
		}
		
		
		//Собирание материалов и их генерация
		for(int i=0;i<genItems.size();i++){
			if(genItems.is_overdisplay(i, map_size_x)) genItems.del(i, map_size_y);
			
			if(!genItems.is_collision(player,i,map_size_x)){
				int qx = 1; //Коэффициент движения по x 
				int qy = 1; //Коэффициент движения по y 
				if(genItems.direction == 0){ //Движение слева направо
					qx = 1;
					qy = 0;
				}else if(genItems.direction == 1){ //Движение справа налево
					qx = -1;
					qy = 0;
				}
					
				
				Sprite sp = genItems.get(i);
				sp.move(qx*0.1*(time/6000),0*qy);
				genItems.set(sp, i, genItems.getType(i));
				window.draw(genItems.get(i));
				
				int item_x = (genItems.x(i)+16)/32;
				int item_y = (genItems.y(i)+16)/32;
				
				// Если предмет попал в сеть.
				if(genItems.in_web(i,objsMap) && (webs.getType(item_x,item_y) == genItems.getType(i) || webs.getType(item_x,item_y) == -1)){
					
					webs.addItem(item_x,item_y,genItems.getType(i),1);
					genItems.del(i, map_size_y);
					
					//std::cout << "count " << i << ": " << webs.getCount(genItems.x(i)/32,genItems.y(i)/32) << endl;
					
					//webs.give();
					//system("pause");
				}
				
			}else{
				int add_random =1;
				
				inv.add(inv.freeSlot(genItems.getType(i)),genItems.getType(i),add_random);
				genItems.del(i, map_size_y);
			}
		}
		
		// Отрисовка инвентаря
		
		int rows = 2; //сколько будем умещать в строке итемов
		int current_row =0;
		int temp = 0;
		
		sf::Sprite sp_select;
		sp_select.setTexture(select_texture);
		int inv_pos_y = inv.getSelectedItem()/rows;
		int inv_x_pos = inv.getSelectedItem()-(inv_pos_y*2);
		sp_select.setPosition((inv_x_pos+1)*32+map_size_x*32, inv_pos_y * 32);
		
		window.draw(sp_select);
		
		
		for(inv_iter=0;inv_iter<inv.slots();inv_iter++){
			int x = ((temp+1)*32)+map_size_x*32;
			int y = current_row*32;
			
			sf::Texture texture;
			if(items.getTexture(inv.getType(inv_iter)) != "")
				texture.loadFromFile(items.getTexture(inv.getType(inv_iter)));
			
			sf::Sprite sp;
			sf::RectangleShape rectangle(sf::Vector2f(0, 0));
			sp.setTexture(texture);
			
			
			//Если это инструмент, то рисуем шкалу прочности
			if(items.getSpec(inv.getType(inv_iter)) == 1){
				//sf::RectangleShape rectangle(sf::Vector2f(5, 32));
				int dur = inv.getDuration(inv_iter);
				int max_dur = inv.getMaxDuration(inv_iter);
				int draw_px = ((100*dur)/max_dur)*0.26;
				
				rectangle.setSize(sf::Vector2f(draw_px, 3));
				rectangle.setFillColor(sf::Color(0, 120, 26));
				rectangle.setOutlineThickness(1);
				rectangle.setOutlineColor(sf::Color(0, 59, 13));
				rectangle.setPosition(x+3,y+27);
				
			}
			
			sp.setPosition(x,y);
			 
			sf::Sprite item;
			
			item.setTexture(inv_texture);
			item.setPosition(x,y);
			
		
			Text text(std::to_string(inv.count(inv_iter)), font, 16);
			text.setColor(Color::Black);
			//text.setStyle(sf::Text::Bold);
			text.setPosition(x,y);
			
			window.draw(item);
			
			if(inv.getType(inv_iter) != -1) window.draw(sp);
			if(inv.getType(inv_iter) != -1 && items.getSpec(inv.getType(inv_iter)) != 1 && inv.count(inv_iter) > 1) window.draw(text);
			if(items.getSpec(inv.getType(inv_iter)) == 1) window.draw(rectangle);
			
			if(temp+1 == rows){
				current_row++;
				temp = 0;
			}else 
				temp++;
		}
		
		
		//Отрисовка Крафт-интерфейса
		for (int i=0;i<3;i++){
			for (int j=0;j<3;j++){
				sf::Sprite craftbox;
				craftbox.setTexture(inv_texture);
				craftbox.setPosition(11*32+(i*32),6*32+(j*32));
				
				int id_item = craft.getIdItem(i,j);
				
				window.draw(craftbox);
				
				if(id_item != -1){
					sf::Texture texture;
					texture.loadFromFile(items.getTexture(id_item));
					
					sf::Sprite item;
					item.setPosition(11*32+(i*32),6*32+(j*32));
					item.setTexture(texture);
					
					window.draw(item);
				}
				
			}
			//Отрисовка окошка
			if(i==2){
				sf::Sprite craftbox;
				craftbox.setTexture(inv_texture);
				craftbox.setPosition(11*32+(1*32),6*32+(3*32));
				
				std::vector <int> craft_res = craft.craft();
				int id_item = craft_res[0];
				
				window.draw(craftbox);
				
				if(id_item != -1){
					sf::Texture texture;
					texture.loadFromFile(items.getTexture(id_item));
					
					sf::Sprite item;
					item.setPosition(11*32+(1*32),6*32+(3*32));
					item.setTexture(texture);
					
					window.draw(item);
					
					if(craft_res[1] > 1){
						sf::Text text;
						text.setFont(font);
						text.setString(std::to_string(craft_res[1]));
						text.setPosition(11*32+(1*32)+20,6*32+(3*32)+16);
						text.setCharacterSize(12);
						text.setFillColor(sf::Color::Blue);
						window.draw(text);
					}
				}
			}
		}
		
		
		//Крафт
		if(mouse.rx() >= 11 && mouse.rx() <=13 && mouse.ry() >=6 && mouse.ry() <= 9){
			int craft_x = mouse.rx()-11;
			int craft_y = mouse.ry()-6;
			
			std::vector <int> craft_res = craft.craft();
			
			//Показываем подсказку
			if(craft.getIdItem(craft_x,craft_y) != -1 && mouse.ry() != 9){
				Boxes box;
				box.setup(mouse.x()+15,mouse.y()+15,items.getDescription(craft.getIdItem(craft_x,craft_y)));
				box.show(&window);
			}else if(mouse.ry() == 9 && mouse.rx() == 12 && craft_res[0] != -1){
				Boxes box;
				box.setup(mouse.x()+15,mouse.y()+15,items.getDescription(craft_res[0]));
				box.show(&window);
			}
			
			if(mouse.left && mouse.ry() != 9){
				if(craft.getIdItem(craft_x,craft_y) != -1){
					inv.add(inv.freeSlot(craft.getIdItem(craft_x,craft_y)),craft.getIdItem(craft_x,craft_y),1);
					craft.setIdItem(craft_x,craft_y,-1);
				}
				
				
				if(craft.getIdAttach() != -1){
					inv.del(craft.getIdAttach(),1);
					craft.setIdItem(craft_x,craft_y,craft.getIdAttach());
					craft.setIdAttach(-1);
				}
				
			}else if(mouse.left && mouse.rx() == 12){
				
				int id_item = craft_res[0];
				
				if(id_item != -1){
					craft.clear();
					std::cout << "Slot " << inv.freeSlot(id_item) << std::endl;
					if(items.getSpec(id_item) == 1)
						inv.add(inv.freeSlot(id_item),id_item,craft_res[1],100,100);
					else
						inv.add(inv.freeSlot(id_item),id_item,craft_res[1]);
				}
			}
		}else if(mouse.left){
			//Очистка перетаскиваемого предмета, если нажато вне зоны крафта
			
			craft.setIdAttach(-1);
		}
		//Показываем итем, если мы его взяли мышкой
		int id_attach = craft.getIdAttach();
		if(id_attach != -1){
			sf::Texture texture;
			texture.loadFromFile(items.getTexture(id_attach));
			
			sf::Sprite item;
			item.setPosition(mouse.x(),mouse.y());
			item.setTexture(texture);
			
			window.draw(item);
		}
		
		//Работа с мышью
		int p_y = (int) (player.y()+16)/32;
		int p_x = (int) (player.x()+16)/32;
		
		//Управление инвентарем мышью
		int mouse_inv_pos_x = (mouse.rx()+1)-(map_size_x+rows);

		if(mouse_inv_pos_x < rows && mouse_inv_pos_x >= 0 && mouse.ry() < 5 && mouse.left){
			
			int mouse_inv_pos = (rows*mouse.ry())+mouse_inv_pos_x;
			
			//Если этот итем уже был выбран и он не пустой, то будем его перетаскивать
			if(inv.getSelectedItem() == mouse_inv_pos && inv.getType(inv.getSelectedItem()) != -1){
				craft.setIdAttach(inv.getType(inv.getSelectedItem()));
			}
			
			inv.setSelectedItem(mouse_inv_pos);
		}else if(mouse_inv_pos_x < rows && mouse_inv_pos_x >= 0 && mouse.ry() < 5){
			//Показываем подсказку при наведеннии на предмет в слоте, если
			//слот не пустой
			
			int mouse_inv_pos = (rows*mouse.ry())+mouse_inv_pos_x;
			
			if(inv.getType(mouse_inv_pos) != -1){
				Boxes box;
				box.setup(mouse.x()+15,mouse.y()+15,items.getDescription(inv.getType(mouse_inv_pos)));
				box.show(&window);
			}
		}
		
		
		// Строительство и использование обьектов (3 на 3 радиус)
		if( ( p_y+1 >= mouse.ry() && p_y-1 <= mouse.ry() ) && ( mouse.rx() >= p_x-1 && mouse.rx() <= p_x+1 ) ){
			//Использование
			
			//Сети
			if(mouse.right && webs.is_web(mouse.rx(),mouse.ry())){
				int type = webs.getType(mouse.rx(),mouse.ry());
				if(type != -1){
					int count = webs.getCount(mouse.rx(),mouse.ry());
					inv.add(inv.freeSlot(type), type,count);
					webs.addItem(mouse.rx(),mouse.ry(),type,-count);
				}
			}
			
			//Копье
			if(mouse.right && shark.getAttackX() == mouse.rx() && shark.getAttackY() == mouse.ry()){
				int id_slot = inv.getSelectedItem();
				
				if(inv.getType(id_slot) == 3){
					int dur = inv.getDuration(id_slot);
					inv.setDuration(id_slot,dur-20);
					shark.setHealth(shark.getHealth()-20);
				}
			}
			
			//Строительство
			
			//Доски
			if(mouse.left && inv.check(1,2) && inv.getType(inv.getSelectedItem()) == 1 && map[mouse.rx()][mouse.ry()] == 1){
				MyMap[to_string(mouse.ry())+":"+to_string(mouse.rx())] = 0;
				map[mouse.rx()][mouse.ry()] = 0;
				inv.del(1,2);
			}
			
			//Сети
			if(mouse.left && inv.check(2,1) && inv.getType(inv.getSelectedItem()) == 2){
				MyMap[to_string(mouse.ry())+":"+to_string(mouse.rx())] = 2;
				objsMap[to_string(mouse.rx())+":"+to_string(mouse.ry())] = 2;
				inv.del(2,1);
				webs.setup(mouse.rx(), mouse.ry());
			}
			
			Sprite select_sp;
			
			if(inv.check(1,2) && inv.getType(inv.getSelectedItem()) == 1 && map[mouse.rx()][mouse.ry()] == 1) select_sp.setTexture(select_texture_on); 
			else select_sp.setTexture(select_texture);
			
			select_sp.setPosition(mouse.rx()*32, mouse.ry()*32);
			window.draw(select_sp);
		}
		
		//Работа с клавиатурой
		
		int key = keyboard.code;
		
		if(key >=48 && key <= 57 && cheat){
			int type = ((char) key)-'0';
			inv.add(inv.freeSlot(type),type,99,100,100);
		}
		
		//Управление инвентарем NumPad'ом (4 - назад, 6 - вперед)
		if(key == 52) inv.addIndex(-1*(time/50000));
		else if(key == 54) inv.addIndex(1*(time/50000));
		
		window.draw(shark.get()); //Отрисовка акулы
		
		//Если акула атакует, то рисуем прогресс
		sf::RectangleShape sharkAttackProgress(sf::Vector2f(0, 0));
		
		if(shark.isAttack){
			int x = shark.getAttackX()*32;
			int y = shark.getAttackY()*32;
			int dur = shark.current_frame;
			int max_dur = shark.frames_attack;
			
			
			int draw_px = ((100*dur)/max_dur)*0.26;
				
			sharkAttackProgress.setSize(sf::Vector2f(draw_px, 3));
			sharkAttackProgress.setFillColor(sf::Color(0, 120, 26));
			sharkAttackProgress.setOutlineThickness(1);
			sharkAttackProgress.setOutlineColor(sf::Color(0, 59, 13));
			sharkAttackProgress.setPosition(x+3,y+11);
			
			window.draw(sharkAttackProgress);
		}
		
		
		window.display();
		
		
		
		
	}
	return 0;
}

