#include <SFML/Graphics.hpp>
#include <vector>
#include <iostream>

class Inv{
	private:
		std::vector<int> items;
		std::vector<int> invtypes;
		std::vector<int> counts;
		std::vector<int> durations;
		std::vector<int> max_durations;
		int count_types = 5;
		int inv_max_slot = 10;
		int max = 99;
		int current_item=0;
	public:
		void start(){
			Items is;
			count_types = is.getCount();
			
			std::cout << count_types << std::endl;
			
			items.reserve(inv_max_slot);
			//textures.reserve(count_types);
			counts.reserve(inv_max_slot);
			durations.reserve(inv_max_slot);
			max_durations.reserve(inv_max_slot);
			
			for(int i=0;i<inv_max_slot;i++)
				items[i] = -1;
			for(int i=0;i<inv_max_slot;i++)
				counts[i] = 0;
			Items items;
			
			count_types = items.getCount()-1;
		}
		void add(int index, int type, int count,int duration = -1,int max_duration = -1){
			if(index < inv_max_slot && type <count_types+1){
				int count_temp = counts[index];
				
				Items is;
				if(is.getSpec(type) == 1)
					count = 1;
				
				int add_seq = 0;
				if(counts[index]+count > 99){
					std::cout << " > 99" << std::endl;
						add_seq = 99-(counts[index]+count);
						
						int free_i = freeSlot(type);
						
						if(free_i != -1){
							items[free_i] = type;
							counts[free_i] = -add_seq;
						}
				}
				
				
				items[index] = type;
				
				if(count_temp == 0)
					counts[index] = 0;
				counts[index] = (counts[index]+count)+add_seq;
				
				durations[index] = duration;
				max_durations[index] = max_duration;
			}
		}
		void del(int type, int count){
				int index = -1;
				
				for(int i=0;i<inv_max_slot;i++){
					if(items[i] == type && counts[i] >=count){
						index = i;
						break;
					}
				}
				if(index != -1){
					if(counts[index]-count >= 0)
						counts[index] = counts[index]-count;
					
					if(counts[index] == 0){
						items[index] = -1;
					}
				}
		}
		bool check(int type, int count){
			int index = -1;
			
			//Находим необходимый слот
			for(int i=0;i<inv_max_slot;i++){
				if(items[i] == type && counts[i] >=count){
					index = i;
					break;
				}
			}
			
			if(counts[index]-count >= 0 && index < inv_max_slot && index != -1)
				return true;
			else
				return false;
		}
		int count(int index){
			if(index < inv_max_slot)
				return counts[index];
			else
				return -1;
		}
		int types(){
			return count_types;
		}
		int slots(){
			return inv_max_slot;
		}
		int freeSlot(int type){
			Items is;
			if(is.getSpec(type) != 1){
				for(int i=0;i<inv_max_slot;i++){
					if(items[i] == type && counts[i] <99) return i;
				}
			}
			for(int j=0;j<inv_max_slot;j++){
				if(items[j] == -1 && counts[j] <99) return j;
			}
			return -1;
		}
		int searchByType(int type, int req_count){
			for(int i=0;i<inv_max_slot;i++){
				if(items[i] == type && counts[i] >= req_count) return i;
			}
			return -1;
		}
		void setSelectedItem(int i){
			if(i <= inv_max_slot && i >=0)
				current_item = i;
		}
		void addIndex(float i){
			if(current_item+((int)i) < inv_max_slot && current_item+((int)i) >=0)
				current_item = current_item + ((int)i);
		}
		int getSelectedItem(){
			return current_item;
		}
		int getType(int index){
			return items[index];
		}
		sf::Texture getTextureItem(int index){
			if(index == 0) return seaweed_texture;
			if(index == 1) return wood_texture;
			
			return select_texture;
		}
		int getDuration(int i){
			return durations[i];
		}
		int getMaxDuration(int i){
			return max_durations[i];
		}
		void setDuration(int i, int dur){
			//if(max_durations[i] >= dur && dur > 0)
			durations[i] = dur;
		
			if(durations[i] <= 0){
				max_durations[i] = 0;
				//invtypes[i] = -1;
				counts[i] = 0;
				items[i] = -1;
			}
				
		}
};
