
class Webs{
	private:
		std::map<std::string,int> webs;
		std::map<int,int> webs_counts;
		std::map<int,int> webs_types;
		int count = 0;
	public:
		void update(std::map<std::string,int> update_objs){
			count = 0;
			for (auto it = update_objs.begin(); it != update_objs.end(); ++it){
				if((*it).second == 2){
					webs[(*it).first] = count;
					//if(webs_counts[count] == 0) webs_counts[count] = 0;
					//if(webs_types[count] == 0) webs_types[count] = -1;
					count++;
				}
			}
		}
		int getCount(int x, int y){
			return webs_counts[webs[std::to_string(x)+":"+std::to_string(y)]];
		}
		int getType(int x, int y){
			return webs_types[webs[std::to_string(x)+":"+std::to_string(y)]];
		}
		void setup (int x, int y){
			webs_types[webs[std::to_string(x)+":"+std::to_string(y)]] = -1;
			webs_counts[webs[std::to_string(x)+":"+std::to_string(y)]] = 0;
			
		}
		void addItem(int x, int y, int type, int count){
			int id = webs[std::to_string(x)+":"+std::to_string(y)];
			int type2 = webs_types[id];
			//if( type2 == type || type2 == -1 ){
				webs_types[id] = type;
				webs_counts[id] = webs_counts[id]+count;
			//}
			if(getCount(x,y) == 0){
				webs_types[id] = -1;
			}
		}
		void give(){
			for (auto it = webs.begin(); it != webs.end(); ++it){
				std::cout << (*it).first << " = " << webs_counts[(*it).second] << endl;
			}
		}
		bool is_web(int x, int y){
			if(webs.count(to_string(x)+":"+to_string(y))>0)
				return true;
			else
				return false;
		}
};