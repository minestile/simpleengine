#include <iostream>
#include <fstream>

using namespace std;

int size_x;
int size_y;
string name;
string dis_not;
int pos_x;
int pos_y;

int main(){
	//cout << "Name your map file: ";
	//cin >> name;
	name = "map";
	
	size_x = 10;
	size_y = 15;
	
	//cout << "Enter player x position ( -1 for default ): ";
	pos_x = -1;
	//cout << endl;
	
	if(pos_x != -1){
		cout << "Enter player y position: ";
		cin >> pos_y;
		cout << endl;
	}else{
		pos_x = 5;
		pos_y = 7;
	}
	
	
	cout << "Disable notifications? (y/n): ";
	cin >> dis_not;
	
	cout << "Creating ..." << endl;
	
	ofstream myfile;
	myfile.open (name+".mst");
	myfile << size_x << " " << size_y << "\n";
	
	myfile << pos_x << " " << pos_y << "\n";
	
	for(int y=0;y<size_y;y++){
		if(dis_not != "y") cout << "Processing map gen for y: " << y << endl;
		for(int x=0;x<size_x;x++){
			if(dis_not != "y") cout << "(" << y << ", " << x << ");" << endl;
			
			//Generation platform for player
			if((pos_x-1==x && pos_y+1==y) || (pos_x==x && pos_y==y) || (pos_x-1==x && pos_y==y) || (pos_x==x && pos_y+1==y)) myfile << y << ":" << x << " " << 0 << "\n";
				else myfile << y << ":" << x << " " << 1 << "\n"; // Filling water
		}

	}
	
	myfile.close();
	
	
	return 0;
}