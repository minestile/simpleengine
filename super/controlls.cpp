
class Control{
	private:
		Player player;
		int speed;
	public:
		Control(Player pl){
			player = pl;
		}
		void left(int time){ player.move(-(speed*time),0); }
		void right(int time){ player.move(speed*time,0); }
		void up(int time){ player.move(0,-(speed*time)); }
		void down(int time){ player.move(0,speed*time); }
		Player get(){
			return player;
		}
};