#include <SFML/Graphics.hpp>
#include <iostream>
#include <vector>
#include <algorithm> 
#include <map> 

using namespace sf;

class GenItems{
	private:
		std::vector<sf::Sprite> items;
		std::vector<int> types;
		int count =0;
		int count_types =0;
		int j=0;
		
	public:
		int direction = 0;
		void setDirection(int dir){
			direction = dir;
		}
		void reserve(int i, int y){
			items.reserve(i);
			types.reserve(2);
			for(int j=0;j<i;j++) del(j,y);
			for(int j=0;j<count_types;j++) types[j] = 0;
			
			count = i;
			
		}
		void add(sf::Sprite sp, int type){
			count++;
			items[count-1] = sp;
			types[count-1] = type;
		}
		void set(sf::Sprite sp, int i, int type){
			items[i] = sp;
			types[count-1] = type;
		}
		void del(int i, int size_y){
			//items.erase (items.begin()+i);
			//int j=i;
			//for(;j<count;j++){
			//	if(j+1 < count) items[j] = items[j+1];
			//}
			//count--;
			
			int random_item = rand() % 2;

			sf::Sprite sp;
			if(random_item == 0){ //Seaweed
				sp.setTexture(seaweed_texture);
			}else if(random_item == 1){ //Wood
				sp.setTexture(wood_texture);
			}
			
			sp.setPosition(( rand() % 5 )*-32,( rand() % size_y )*32);
			
			items[i] = sp;
			types[i] = random_item;
		}
		sf::Sprite get(int i){
			return items[i];
		}
		int getType(int i){
			return types[i];
		}
		int size(){
			return count;
		}
		bool is_overdisplay(int i, int size_x){
			sf::Vector2f item = get(i).getPosition();
			float x2=item.x;
			float y2=item.y;
			
			int size1, size2 = 32;
			
			if(x2+size2 >= size_x*32) return true;
			else return false;
		}
		bool is_collision(Player p, int i, int size_x){
			float x1 = p.x()+16;
			float y1 = p.y()+16;
			
			sf::Vector2f item = get(i).getPosition();
			float x2=item.x;
			float y2=item.y;
			
			int size1, size2 = 32;
			
			//if(x2+size2 >= size_x*32) return true;
			
			bool XColl=false;
			bool YColl=false;
			
			if ((x1 + size1 >= x2) && (x1 <= x2 + size2)) XColl = true;
			if ((y1 + size1 >= y2) && (y1 <= y2 + size2)) YColl = true;

			if (XColl&YColl){return true;}
			return false;
		}
		bool in_web(int i, std::map<std::string,int> objs){
			sf::Vector2f item = get(i).getPosition();
			int x2=item.x;
			int y2=item.y;
			
			for (auto it = objs.begin(); it != objs.end(); ++it){
				
				if((*it).second == 2 && (*it).first == to_string(x2/32)+":"+to_string(y2/32)){
					//cout << to_string(x2/32)+":"+to_string(y2/32) << endl;
					return true;
				}
			}
			return false;
		}
		float x(int i){
			sf::Vector2f item = get(i).getPosition();
			return item.x;
		}
		float y(int i){
			sf::Vector2f item = get(i).getPosition();
			return item.y;
		}

};
