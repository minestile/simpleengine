#include <iostream>
#include <map>
#include "map_controll.cpp"


using namespace std;

//int map[2][2];
map <string,int> MyMap;

int map_gen_x = 0;
int map_gen_y = 0;

int main(){
	
	MyMap = select_map();
	
	
	int map[map_size_x][map_size_y];
	
	for (auto it = MyMap.begin(); it != MyMap.end(); ++it){
		auto v = explode((*it).first, ':');
		int x = atoi( v[0].c_str() );
		int y = atoi( v[1].c_str() );	
		map[x][y] =  (int) (*it).second;
	}
	
	
	for(map_gen_y = 0; map_gen_y < map_size_y; map_gen_y++)
		for(map_gen_x = 0; map_gen_x < map_size_x; map_gen_x++)
			cout << map[map_gen_x][map_gen_y] << endl;
	
	system("pause");
	
	return 0;
}