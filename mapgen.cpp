#include <iostream>
#include <fstream>

using namespace std;

int size_x;
int size_y;
string name;
string dis_not;
string pos_x;
string pos_y;

int main(){
	cout << "Name your map file: ";
	cin >> name;
	cout << "Enter map x size: ";
	cin >> size_x;
	cout << "Enter map y size: ";
	cin >> size_y;
	cout << endl;
	
	cout << "Enter player x position (n - for default): ";
	cin >> pos_x;
	cout << endl;
	
	if(pos_x != "n"){
		cout << "Enter player y position: ";
		cin >> pos_y;
		cout << endl;
	}else{
		pos_x = "1";
		pos_y = "1";
	}
	
	
	cout << "Disable notifications? (y/n): ";
	cin >> dis_not;
	
	cout << "Creating ..." << endl;
	
	ofstream myfile;
	myfile.open (name+".mst");
	myfile << size_x << " " << size_y << "\n";
	
	myfile << atoi(pos_x.c_str()) << " " << atoi(pos_y.c_str()) << "\n";
	
	for(int y=0;y<size_y;y++){
		if(dis_not != "y") cout << "Processing map gen for y: " << y << endl;
		for(int x=0;x<size_x;x++){
			if(y == 0 || y == size_y-1 || x == 0 || x == size_x-1){
				if(dis_not != "y") cout << "(" << y << ", " << x << ");" << endl;
				myfile << y << ":" << x << " " << 1 << "\n";
			}else{
				if(dis_not != "y") cout << "(" << y << ", " << x << ");" << endl;
				myfile << y << ":" << x << " " << 0 << "\n";
			}
			
		}
	}
	
	myfile.close();
	
	
	return 0;
}