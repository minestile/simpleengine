#include <iostream>
#include <fstream>
#include <SFML/Graphics.hpp>
#include "windows.h"
#include <sstream>
#include "map_controll.cpp"

using namespace std;

std::string IStr(int a)
{
    ostringstream temp;
    temp<<a;
    return temp.str();
}


using namespace sf;

map <string,int> MyMap;

int cx = 0;
int cy = 0;

int speed = 17000;

void change_state_block(int num, int x, int y){
	int avail = 1;
	
	int iNum = (int)num - 48;
	
	if(iNum <=avail){
		MyMap[IStr(x)+":"+IStr(y)] = iNum;
	}
	
	cout << "Char: " << iNum << endl; 
}



int main (){
	MyMap = select_map();
	
	RenderWindow window(VideoMode(400,400), "Test!");
	
	Texture t;
	t.loadFromFile("res/select.png");
	
	Sprite s;
	s.setTexture(t);
	s.setPosition(cx,cy);
	
	Font font;//шрифт 
	font.loadFromFile("res/slkscr.ttf");
	Text text("", font, 12);
	text.setColor(Color::Red);
	text.setStyle(sf::Text::Bold | sf::Text::Underlined);
	
	
	Clock clock;
	
	while(window.isOpen()){
		
		s.setPosition(cx*32,cy*32);
		text.setString(IStr(MyMap[IStr(cx)+":"+IStr(cy)]));
		text.setPosition(cx*32+12, cy*32+4);
		
		
		float time = clock.getElapsedTime().asMicroseconds();
		clock.restart();
		time = time/speed;
		
		
		window.clear();
		
		Event event;
		while(window.pollEvent(event)){
			if(event.type == Event::Closed)
				window.close();
			if (event.type == sf::Event::TextEntered){
				if (event.text.unicode <= 57 && event.text.unicode >= 48)
					change_state_block(event.text.unicode, cx, cy);
				if(event.text.unicode == 13)
					save(MyMap);
			}
				
			
		}
		
		int rel = 0;
		for (auto it = MyMap.begin(); it != MyMap.end(); ++it){
			//cout << (*it).first << " : " << (*it).second << endl;
			
			auto v = explode((*it).first, ':');
			int x = atoi( v[0].c_str() );
			int y = atoi( v[1].c_str() );
			
			sf::Texture t2;
			if((*it).second == 1){ //Block
				
				t2.loadFromFile("res/block.png");
			}else if((*it).second == 0) // dirt
				t2.loadFromFile("res/dirt.png");
			
			
			
			
			
			sf::Sprite s2;
			s2.setTexture(t2);
			s2.setPosition(x*32,y*32);
			window.draw(s2);
		}
		
		if (Keyboard::isKeyPressed(Keyboard::Left)){
			cx = cx-1;
			Sleep(100);
		}
		if (Keyboard::isKeyPressed(Keyboard::Right)){
			cx = cx+1;
			Sleep(100);
		}
		if (Keyboard::isKeyPressed(Keyboard::Up)){
			cy = cy-1;
			Sleep(100);
		}
		if (Keyboard::isKeyPressed(Keyboard::Down)){
			cy = cy+1;
			Sleep(100);
		}
		
		//cout << cx << " " << cy << endl;
		
		window.draw(s);
		window.draw(text);
		
		window.display();
	}
	
	
}